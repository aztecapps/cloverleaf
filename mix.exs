defmodule Cloverleaf.Mixfile do
  use Mix.Project

  def project do
    [app: :cloverleaf,
     version: "0.0.1",
     elixir: "~> 1.4",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix] ++ Mix.compilers,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [mod: {Cloverleaf.Application, []},
     extra_applications: [:logger, :runtime_tools, :gproc]]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  defp deps do
    [{:phoenix, "~> 1.3.0-rc"},
     {:cowboy, "~> 1.0"},
     {:amqp, "~> 0.2.1"},
     {:uuid, "~> 1.1"},
     {:poolboy, "~> 1.5.1"},
     {:gproc, "~> 0.6.1"},
     {:cors_plug, "~> 1.2"}
    ]
  end
end
