FROM elixir

ENV DEBIAN_FRONTEND=noninteractive

RUN mix local.hex --force

RUN mix local.rebar --force

RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez

WORKDIR /app
ADD . /app

RUN mix deps.get
RUN mix compile
