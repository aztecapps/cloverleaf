# APIv3

Setting up a development environment requires installation of 2 applications from repositories as well as rabbitmq installed. This is a distributed system, so the repositories and rabbitmq can be installed all on the same server, or on separate servers.

### Dev Environment Setup

##### RabbitMQ Setup
Install RabbitMQ
> Note: update rabbitmq-server-3.6.10-1.noarch.rpm to the current version 
```sh
sudo yum -y update
sudo yum install wget
sudo yum -y install epel-release
sudo wget https://packages.erlang-solutions.com/erlang/esl-erlang/FLAVOUR_1_general/esl-erlang_20.1-1~centos~7_amd64.rpm
sudo yum install erlang
sudo yum install esl-erlang
sudo rpm -Uvh --replacefiles esl-erlang_20.1-1~centos~7_amd64.rpm
sudo rpm --import https://www.rabbitmq.com/rabbitmq-release-signing-key.asc
sudo wget https://dl.bintray.com/rabbitmq/rabbitmq-server-rpm/rabbitmq-server-3.6.12-1.el7.noarch.rpm
sudo yum -y install rabbitmq-server-3.6.12-1.el7.noarch.rpm
sudo systemctl enable rabbitmq-server.service
sudo systemctl start rabbitmq-server.service
```

Install dependencies for first repository
```sh
sudo yum -y update
cd /usr/bin
sudo mkdir elixir
cd /usr/bin/elixir
sudo wget https://github.com/elixir-lang/elixir/releases/download/v1.5.2/Precompiled.zip
sudo yum install unzip
sudo unzip Precompiled.zip
/usr/bin/elixir/bin/elixir -v
sudo vi /etc/profile
```
Scroll down to the end and add this line
```sh
export PATH="$PATH:/usr/bin/elixir/bin:/usr/bin/node-v6.1.0-linux-x64/bin"
```
```sh
mix local.hex --force
mix local.rebar --force
mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez
```

##### APIv3-Cloverleaf Setup
Clone the first repo
```sh
cd /your/install/path/goes/here
sudo yum install git
git clone https://gitlab.opadev.dol.gov/opatechteamdev/APIv3-Cloverleaf.git
cd apiv3-cloverleaf
mix deps.get
mix compile
mix phx.server
```

Edit config file
```sh
vi config/dev.exs
```
* In the route_manager and api_manager sections, edit the hostname to point to the IP of your rabbitmq installation(or localhost if it is installed locally).

* Save dev.exs

Start the phoenix server
```sh
cd ../
mix phx.server
```

##### APIv3-Microservice Setup
Install python3 & ODBC for second repository
```sh
sudo yum -y update
sudo yum -y install yum-utils
sudo yum -y groupinstall development
sudo yum -y install https://rhel7.iuscommunity.org/ius-release.rpm
sudo yum -y install python36u
sudo yum -y install python36u-pip
sudo yum -y install python36u-devel
sudo yum -y install libiodbc unixODBC.x86_64 unixODBC-devel
cd /usr/bin
sudo ln -s python3.6 python3

```

Clone the second repo and install application dependencies
```sh
cd /your/install/path/goes/here
sudo yum install git
git clone https://gitlab.opadev.dol.gov/opatechteamdev/APIV3-Python-Microservices.git
cd apiv3-python-microservices
sudo yum remove mariadb-libs
sudo yum install -y mysql-devel
sudo pip3.6 install --no-cache-dir -r requirements.txt
```

Edit config file
```sh
cp settings.yaml.default settings.yaml
vi settings.yaml
```
* Update the API section to use the connection settings of the mysql database that holds your connection strings(currently this uses the APIv2 connection string database).
* In the API section, the databases key stores the IDs of each database you'd like to create a connection to. If you want to access a database, you must enter its ID from the connection string database here.
* Update the Message Store section to point to the IP of your rabbitmq installation(or localhost if it is installed locally).
*Use these settings to run locally (temporary)

api:
    driver: mysql
    username: adminui
    password: adminui
    host: dds_lde_db_mys.opadev.dol.gov
    port: '3306'
    database: restdb
    table: connection_strings
    primary_key: id
    #databases is a list of ids. Those ids will be the only ones selected from the connection strings table
    databases:
        - 313
        - 314

message_store:
    host: localhost
    port: 5672
    username: guest
    password: guest
    virtual_host: /
    router: routes

* Save settings.yaml

Start the python microservices
```sh
python3 start_services.py
```

### Docker Installation
The following shows how to install docker, which is needed on any server you wish to create docker boxes on.
```sh
sudo yum -y update
sudo yum -y install curl
curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker <your user>
sudo systemctl enable docker
sudo systemctl start docker
docker login (username is opadev)
```

### Creating a Docker Image
The following shows how to turn your dev environment into a docker image for deployment into QA or production.

Exporting APIv3-Cloverleaf
> Version must be in 1.1.1 format
```sh
cd /path/to/apiv3-cloverleaf
docker build -t opadev/apiv3-cloverleaf .
docker tag opadev/apiv3-cloverleaf opadev/apiv3-cloverleaf:<version>
docker push opadev/apiv3-cloverleaf:<version>
docker push opadev/apiv3-cloverleaf
```

Exporting APIv3-Python-Microservices
> Version must be in 1.1.1 format
```sh
cd /path/to/apiv3-python-microservices
docker build -t opadev/apiv3-microservices .
docker tag opadev/apiv3-microservices opadev/apiv3-microservices:<version>
docker push opadev/apiv3-microservices:<version>
docker push opadev/apiv3-microservices
```

### Configuring Docker Swarm
> The following explains how to configure a docker swarm for QA and production deployment. This assumes docker is installed on all servers in the swarm. See above for installation instructions.

Configure firewall on all servers(run these commands on every server in swarm, but only run the line to open port 2377 on your swarm manager)
```sh
sudo firewall-cmd --add-port=2376/tcp --permanent
sudo firewall-cmd --add-port=2377/tcp --permanent
sudo firewall-cmd --add-port=7946/tcp --permanent
sudo firewall-cmd --add-port=7946/udp --permanent
sudo firewall-cmd --add-port=4789/udp --permanent
sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --add-port=443/tcp --permanent
sudo firewall-cmd --reload
sudo systemctl restart docker
```

Configure swarm
> Any server can be designated swarm manager, but there can only be one.
```sh
docker swarm init (only run on swarm manager)
```
This will give you a command to cut and paste to execute on every other server in your swarm. Do so. Your swarm should now be configured.

### Deploying To QA and Production
> The docker-compose.yml file is included in the APIv3-Cloverleaf repository, but can be moved anywhere on your swarm managing server.
```sh
cd /path/to/docker-compose.yml
docker stack deploy --compose-file docker-compose.yml apiv3
```