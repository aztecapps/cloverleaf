use Mix.Config

config :cloverleaf, Cloverleaf.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/Yrz/5BS+U7uEupMa040iSHzxpSr4QZ9gFRsGjRltwhMY3j/Ug0BnK9xJyIW8eUw",
  render_errors: [view: Cloverleaf.Web.ErrorView, accepts: ~w(json)]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

import_config "#{Mix.env}.exs"
