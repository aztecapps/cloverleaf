use Mix.Config

config :cloverleaf, Cloverleaf.Web.Endpoint,
  on_init: {Cloverleaf.Web.Endpoint, :load_from_system_env, []},
  url: [host: "localhost", port: 80]

config :logger, level: :info

config :cloverleaf, :route_messager,
  hostname: "amqp://guest:guest@messager",
  queue: "routes",
  pool: :routes_pool

config :cloverleaf, :api_messager,
  hostname: "amqp://guest:guest@messager",
  queue: "api",
  pool: :api_pool
