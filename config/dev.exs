use Mix.Config

config :cloverleaf, Cloverleaf.Web.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

config :logger, :console, format: "[$level] $message\n"

config :phoenix, :stacktrace_depth, 20

config :cloverleaf, :route_messager,
  hostname: "amqp://guest:guest@messager",
  queue: "routes",
  pool: :routes_pool

config :cloverleaf, :api_messager,
  hostname: "amqp://guest:guest@messager",
  queue: "api",
  pool: :api_pool
