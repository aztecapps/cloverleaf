use Mix.Config

config :cloverleaf, Cloverleaf.Web.Endpoint,
  http: [port: 4001],
  server: false

config :logger, level: :warn
