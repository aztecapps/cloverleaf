defmodule Cloverleaf.Routes do

  @cache "routes"

  def add_new_route(route) do
    Cloverleaf.Cache.start_link(@cache)
    Cloverleaf.Cache.add_value(@cache, route)
  end

  def get_routes do
    Cloverleaf.Cache.start_link(@cache)
    Cloverleaf.Cache.get_values(@cache)
  end
end
