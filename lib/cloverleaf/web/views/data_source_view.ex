defmodule Cloverleaf.Web.DataSourceView do
  use Cloverleaf.Web, :view

  def render("index.json", %{message: message}), do: message
end
