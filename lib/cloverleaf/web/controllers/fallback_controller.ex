defmodule Cloverleaf.Web.FallbackController do
  use Cloverleaf.Web, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(Cloverleaf.Web.ErrorView, :"404")
  end
end
