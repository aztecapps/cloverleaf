defmodule Cloverleaf.Web.DataSourceController do
  use Cloverleaf.Web, :controller

  action_fallback Cloverleaf.Web.FallbackController

  def index(conn, _params) do
    render conn, "index.json", message: conn.assigns[:message]
  end
end
