defmodule Cloverleaf.Web.Router do
  use Cloverleaf.Web, :router
  use Plug.ErrorHandler

  pipeline :api do
    plug :accepts, ["json"]
    plug Cloverleaf.Web.Plug.ValidateRoute
    plug Cloverleaf.Web.Plug.DataRequest
  end

  scope "/api/v3", Cloverleaf.Web do
    pipe_through :api
    get "/*route", DataSourceController, :index
    post "/*route", DataSourceController, :index
    put "/*route", DataSourceController, :index
    patch "/*route", DataSourceController, :index
    delete "/*route", DataSourceController, :index
  end

  defp handle_errors(%Plug.Conn{status: 500} = conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    send_resp(conn, 500, Poison.encode!(%{errors: %{detail: "Internal server error"}}))
  end

  defp handle_errors(_, _), do: nil 

end
