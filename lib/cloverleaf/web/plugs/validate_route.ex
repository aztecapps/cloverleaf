defmodule Cloverleaf.Web.Plug.ValidateRoute do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    valid_route = validate_route(hd(conn.params["route"]))
    response(conn, valid_route)
  end

  defp response(conn, :true), do: conn

  defp response(conn, :false) do
    conn
    |> Cloverleaf.Web.FallbackController.call({:error, :not_found})
    |> halt
  end

  defp validate_route(route) do
    Cloverleaf.Cache.start_link("routes")
    routes = Cloverleaf.Cache.get_values("routes")
    route in routes
  end
end
