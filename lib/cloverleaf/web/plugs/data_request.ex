defmodule Cloverleaf.Web.Plug.DataRequest do
  import Plug.Conn
  alias Cloverleaf.Messager.Supervisor, as: Messager

  def init(opts), do: opts

  def call(conn, _opts) do
    route = get_route(conn.params["route"])
    message = Messager.publish_to_pool(:api_messager, route, format_params(conn, conn.params))
    assign(conn, :message, message)
  end

  def format_params(conn, params) do
    params
    |> Map.put("id", get_record_id(params["route"]))
    |> Map.delete("route")
    |> Map.put("method", conn.method)
  end

  def get_route(url) do
    hd(url)
  end

  def get_record_id([_route, id]) do
    id
  end

  def get_record_id(_route) do
    nil
  end
end
