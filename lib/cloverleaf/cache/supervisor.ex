defmodule Cloverleaf.Cache.Supervisor do
  use Supervisor

  def start_link, do: Supervisor.start_link(__MODULE__, [], name: __MODULE__)

  def init(_) do
    children = [
      worker(Cloverleaf.Cache, ["routes"])
    ]
  
    supervise(children, strategy: :simple_one_for_one)
  end
end
