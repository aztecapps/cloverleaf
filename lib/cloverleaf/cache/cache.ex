defmodule Cloverleaf.Cache do
  use GenServer

  def start_link(name) do
    GenServer.start_link(__MODULE__, [], name: via_key(name))
  end

  def init(values) do
    {:ok, values}
  end

  def via_key(key) do
    {:via, :gproc, {:n, :l, {:keys, key}}}
  end

  def add_value(key, value) do
    GenServer.cast(via_key(key), {:add_value, value})
  end

  def get_values(key) do
    GenServer.call(via_key(key), :get_values)
  end

  def handle_cast({:add_value, value}, key) do
    {:noreply, Enum.uniq([value | key])}
  end

  def handle_call(:get_values, _from, key) do
    {:reply, key, key}
  end
end
