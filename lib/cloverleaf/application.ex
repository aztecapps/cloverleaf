defmodule Cloverleaf.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(Cloverleaf.Web.Endpoint, []),
      supervisor(Cloverleaf.Cache.Supervisor, []),
      supervisor(Cloverleaf.Messager.Supervisor, [Application.get_env(:cloverleaf, :route_messager)], [id: :route_messager]),
      supervisor(Cloverleaf.Messager.Supervisor, [Application.get_env(:cloverleaf, :api_messager)], [id: :api_messager])
    ]

    opts = [strategy: :one_for_one, name: Cloverleaf.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
