defmodule Cloverleaf.Messager.Consumer do
  require Logger
  use AMQP
  import Cloverleaf.Routes

  def parse_payload(payload), do: Poison.Parser.parse!(payload)

  def consume(channel, tag, redelivered, routing_key, payload) do
    Basic.ack(channel, tag)
    try do
      Logger.info("[Messager.Consumer] message received")
      payload = parse_payload(payload)
      process_message(payload, routing_key)
      Logger.info("[Messager.Consumer] message processed")
    rescue
      exception ->
        Logger.error("[Messager.Consumer] error receiving message")
    end
  end

  def wait_for_response(_channel, correlation_id) do
    receive do
      {:basic_deliver, payload, %{correlation_id: ^correlation_id}} ->
        parse_payload(payload)
    end
  end

  defp process_message(message, "routes"), do: add_new_route(message["route"])

  defp process_message(_message, "api"), do: :ok
end