defmodule Cloverleaf.Messager do
  require Logger
  use AMQP
  use GenServer

  def start_link(args), do: :gen_server.start_link(__MODULE__, args, [])

  def init(args), do: connect(args)

  defp connect(args) do
    %{hostname: hostname, queue: queue} = Map.new(args)
    case Connection.open(hostname) do
      {:ok, conn} -> 
        Process.monitor(conn.pid)
        {:ok, channel} = Channel.open(conn)
        Basic.qos(channel, prefetch_count: 1)
        register_consumer_queue(channel, queue)
        Logger.info("[Messager] listening on queue #{queue} for messages")
        {:ok, channel}
      {:error, _} ->
        Logger.info("[Messager] error connecting to queue #{queue}")
        :timer.sleep(10000)
        connect(args)
      end
  end

  def register_consumer_queue(channel, queue="routes") do
    Queue.declare(channel, queue, [durable: :true])
    Basic.consume(channel, queue)
  end

  def register_consumer_queue(channel, queue="api") do
    Queue.declare(channel, queue, [durable: :true])
  end

  def stop(pid), do: GenServer.call(pid, :stop)

  def publish(pid, routing_key, message), do: GenServer.call(pid, {:publish, routing_key, message})

  def handle_call(:stop, _from, state), do: {:stop, :normal, state}

  def handle_call({:publish, routing_key, message}, _conn, chan) do
    return_message = Cloverleaf.Messager.Publisher.publish(chan, routing_key, message)
    {:reply, return_message, chan}
  end

  def handle_info({:basic_consume_ok, %{consumer_tag: _consumer_tag}}, chan), do: {:noreply, chan}

  def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, chan), do: {:stop, :normal, chan}

  def handle_info({:basic_cancel_ok, %{consumer_tag: _consumer_tag}}, chan), do: {:noreply, chan}

  def handle_info({:basic_deliver, payload, %{delivery_tag: tag, redelivered: redelivered, routing_key: routing_key}}, chan) do
    spawn fn -> Cloverleaf.Messager.Consumer.consume(chan, tag, redelivered, routing_key, payload) end
    {:noreply, chan}
  end

  def handle_info({:DOWN, _, :process, _pid, _reason}, _) do
    {:noreply, ""}
  end

  def handle_cast(_msg, state), do: {:noreply, state}
end
