defmodule Cloverleaf.Messager.Publisher do
  require Logger
  use AMQP

  def encode_payload(payload), do: Poison.encode!(payload)

  def publish(channel, routing_key, message) do
    Logger.info("[Messager.Publisher] message received")
    {:ok, %{queue: return_queue}} = Queue.declare(channel, "", [exclusive: true, auto_delete: true])
    Basic.consume(channel, return_queue, nil, [no_ack: true, exclusive: true])
    correlation_id = process_message(channel, routing_key, return_queue, message)
    message = Cloverleaf.Messager.Consumer.wait_for_response(channel, correlation_id)
    Logger.info("[Messager.Publisher] message published to end user")
    message
  end

  defp process_message(channel, routing_key, return_queue, message) do
    correlation_id = get_correlation_id()
    Basic.publish(channel, "", routing_key, encode_payload(message), [correlation_id: correlation_id, reply_to: return_queue])
    correlation_id
  end

  defp get_correlation_id() do
    UUID.uuid4(:default)
  end
end
