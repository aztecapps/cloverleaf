defmodule Cloverleaf.Messager.Supervisor do
  use Supervisor

  def start_link(args), do: :supervisor.start_link(__MODULE__, args)

  def init(args) do
    pool_options = [
      name: {:local, args[:pool]},
      worker_module: Cloverleaf.Messager,
      size: 5,
      max_overflow: 2
    ]

    children = [
      :poolboy.child_spec(args[:pool], pool_options, args),
    ]

    supervise(children, strategy: :one_for_one)
  end

  def publish_to_pool(service, queue, query) do
    service = Application.get_env(:cloverleaf, service)
    :poolboy.transaction(service[:pool], fn(pid) -> :gen_server.call(pid, {:publish, queue, query}) end, 5000)
  end
end
